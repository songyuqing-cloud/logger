<?php

namespace Ntsoft\Logger;

use Monolog\Logger;

class LogMonolog
{
    public function __invoke(array $config)
    {
        $logger = new Logger('ntsoft_logs');
        $logger->pushHandler(new LogHandler());
        $logger->pushProcessor(new LogProcessor());

        return $logger;
    }
}
